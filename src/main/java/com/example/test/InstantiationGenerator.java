package com.example.test;

import java.io.File;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.ACC_PUBLIC;
import static org.objectweb.asm.Opcodes.ACC_STATIC;
import static org.objectweb.asm.Opcodes.ARETURN;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.NEW;

public class InstantiationGenerator extends ClassGeneratorBase {
    public static final String FACTORY_METHOD_NAME = "create";

    protected String classToInstantiate;
    protected String classToInstantiateInternalName;
    protected boolean invokeObjectConstructor = false;

    public InstantiationGenerator(File baseDir, String className, String classToInstantiate) {
        super(baseDir, className);
        setClassToInstantiate(classToInstantiate);
    }

    public InstantiationGenerator(File baseDir, String className, String superClassName, String classToInstantiate) {
        super(baseDir, className, superClassName);
        setClassToInstantiate(classToInstantiate);
    }

    public InstantiationGenerator(File baseDir, String className, String superClassName, String[] implementedInterfaces, String classToInstantiate) {
        super(baseDir, className, superClassName, implementedInterfaces);
        setClassToInstantiate(classToInstantiate);
    }

    public boolean isInvokeObjectConstructor() {
        return invokeObjectConstructor;
    }

    public void setInvokeObjectConstructor(boolean invokeObjectConstructor) {
        this.invokeObjectConstructor = invokeObjectConstructor;
    }

    public void setClassToInstantiate(String classToInstantiate) {
        this.classToInstantiate = validateClassName(classToInstantiate);
        this.classToInstantiateInternalName = classToInstantiate.replace('.', '/');
    }

    @Override
    protected void generateClass(ClassVisitor classVisitor) throws Exception {
        generateDefaultConstructor(classVisitor);

        generateInstantiateMethod(classVisitor);
    }

    protected void generateInstantiateMethod(ClassVisitor classVisitor) {
        MethodVisitor methodVisitor = classVisitor.visitMethod(ACC_PUBLIC | ACC_STATIC,
                                                               FACTORY_METHOD_NAME,
                                                               Type.getMethodDescriptor(Type.getObjectType(classToInstantiateInternalName)),
                                                               null,
                                                               null);
        methodVisitor.visitCode();
        methodVisitor.visitTypeInsn(NEW, classToInstantiateInternalName);
        if (invokeObjectConstructor) {
            methodVisitor.visitInsn(DUP);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, Type.getInternalName(Object.class), "<init>", Type.getMethodDescriptor(Type.VOID_TYPE), false);
        }
        methodVisitor.visitInsn(ARETURN);

        methodVisitor.visitMaxs(1, 0);
        methodVisitor.visitEnd();
    }
}
