package com.example.test;

import java.io.File;

import org.objectweb.asm.ClassVisitor;

public class NoConstructorClassGenerator extends ClassGeneratorBase {

    public NoConstructorClassGenerator(File baseDir, String className) {
        super(baseDir, className);
    }

    @Override
    protected void generateClass(ClassVisitor classVisitor) throws Exception {
    }
}
